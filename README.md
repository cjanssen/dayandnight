Day and Night
=============

    Berlin Mini Game Jam August 2014
    keyboard + gamepad supported (see in-game instructions)


Credits
-------

    Art - Kirill Krysov
    Code - Christiaan Janssen
    Music - "Her name is Cindy" by Noisy Sundae
    Font - "Cecily" by someshinzz
    Sound FX - SFXR + Audacity
    Engine - Löve2D 0.9.1
    License - GNU GPL V3.0
    
    
Copyright
---------

    Music licensed under Creative Commons Attribution Non-commercial Share-alike (CC-BY-NC-SA).  Original in: http://www.newgrounds.com/audio/listen/607805
    Font licensed under Freeware (commercial works allowed)
    Art+Code licensed under GNU GPL V3.0

