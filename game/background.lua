function initBackground()
	bgday = love.graphics.newImage("img/bgday.png")
	bgnight = love.graphics.newImage("img/bgnight.png")
	splash = love.graphics.newImage("img/splash.png")
end

function drawBackground()
	love.graphics.setColor(255,255,255)
	local currentBg = Maps.current == Maps.day and bgday or bgnight

	local iw,ih = currentBg:getWidth()*camera.scale.x,currentBg:getHeight()*camera.scale.y
	local bx,by = (-camera.pos.x*0.5*camera.scale.x)%iw,(-camera.pos.y*0.5*camera.scale.y)%ih
	local bw,bh = math.floor(screenWidth/iw), math.floor(screenHeight/ih)

	for xx=-1,bw do
		for yy=-1,bh do
			love.graphics.draw(currentBg,bx+xx*iw,by+yy*ih, 0, camera.scale.x, camera.scale.y)
		end
	end
end

function drawSplash()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(splash)
end
