function initCamera()
	camera = {
		scale = Vector(1/2.5, 1/2.5),
		-- scale = Vector(1,1),
		pos = Vector(0,0),
		offs = Vector(0,0),
		dest = Vector(0,0),
		speed = 1100,
		viewPos = Vector(0,0),
	}
end

function setFullscreen(fs)
	local ok = love.window.setFullscreen(fs, "desktop")
	if not ok then
		saveSettings()
	end
end

function isFullscreen()
	return love.window.getFullscreen()
end

function switchFullscreen()
	local ok = love.window.setFullscreen(not love.window.getFullscreen(), "desktop")
	if ok then
		saveSettings()
	end
end

function setZoom(decidedZoom)
	camera.scale = Vector(decidedZoom,decidedZoom)
end

function centerCameraInPlayer()
	local plapo = player.pos + player.size*0.5
	local ss = Vector(screenWidth, screenHeight)
	camera.pos = plapo
	camera.viewPos = camera.pos - ss*0.5
	-- run 1 update loop with really high dt to avoid anim
	updateCamera(100)
end

function updateCamera(dt)
	local ss = Vector(screenWidth/camera.scale.x, screenHeight/camera.scale.y)
	local plapo = player.pos + player.size*0.5

	local sh = ss * 0.5

	if player.lookMode and not player.lookMode:isZero() then
		-- look mode
		camera.dest = plapo + player.lookMode ^ ss*0.4
	else
		-- normal view
		if player.vel.x > 0 then camera.offs.x = ss.x*0.25 end
		if player.vel.x < 0 then camera.offs.x = -ss.x*0.25 end
		local sq = ss/6
		camera.dest.y = math.clamp(plapo.y - sq.y, camera.dest.y, plapo.y+sq.y)
		camera.dest.x = plapo.x+camera.offs.x
		camera.dest.x = math.clamp(sh.x,camera.dest.x,Maps.current.totalWidth-sh.x)
	end

	local dd = camera.dest - camera.pos
	if dd:mod() < camera.speed*dt then
		camera.pos = camera.dest:copy()
	else
		camera.pos = camera.pos + dd:norm() * camera.speed*dt
	end

	local maxPos = Vector(Maps.current.totalWidth, Maps.current.totalHeight) - sh
	camera.pos = camera.pos:clamp(sh, maxPos)

	camera.viewPos = camera.pos - sh
end