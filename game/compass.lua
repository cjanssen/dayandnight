function initCompass()
    Compass = {
        visible = false,
        wasVisible = false,
        pos = Vector(0,0),
        dest = Vector(0,0),
        radius = 0,
        phase = 0,
        period = 1.15,
        width = 9,
        opacity = 1,
    }
end

function resetCompass()
	Compass.visible = false
end

function updateCompass(dt)
    Compass.visible = false

    if Maps.current == Maps.day then
        if player.lookMode then
            Compass.visible = true
        end

        if Compass.visible then
            -- find closest object
            Compass.pos = player.pos + player.size*0.5

            local origDist = Maps.day.totalWidth * Maps.day.totalHeight
            local minDist = origDist

            local minPos = false
            for i=1,4 do
                if not player.objectsPicked.list[i] then
                    local ol = Maps.day.objLocations[i]
                    local objPos = Vector((ol.x - 0.5) * Maps.day.tilewidth, (ol.y - 0.5) * Maps.day.tileheight)
                    local objDist = (objPos - Compass.pos):mod()

                    if objDist < minDist then
                        minDist = objDist
                        minPos = objPos
                    end
                end
            end

            if minPos then
                Compass.dest = minPos
                local md = math.max(Maps.day.totalWidth, Maps.day.totalHeight)

                Compass.radius = 90 * math.sqrt((minDist + md*0.15)/md)
                local factor = 0.60+0.10*(1-Compass.radius/100)

                -- generate polygon
                local startAng = (Compass.dest-Compass.pos):angle() + math.pi*0.5
                Compass.poly = {}
                local segments = 10
                for i=0,segments do
                    table.insert(Compass.poly, Compass.pos.x + math.cos(startAng + i*math.pi/segments) * Compass.radius)
                    table.insert(Compass.poly, Compass.pos.y + math.sin(startAng + i*math.pi/segments) * Compass.radius)
                end
                local p1 = (Compass.pos * factor + Compass.dest * (1-factor))
                table.insert(Compass.poly, p1.x)
                table.insert(Compass.poly, p1.y)

                Compass.phase = (Compass.phase + dt*2*math.pi/Compass.period) % (2*math.pi)
                Compass.width = 7 - 5 * math.cos(Compass.phase)
                Compass.opacity = 0.45 + 0.25 * math.cos(Compass.phase)
            else
                Compass.visible = false
            end
        else
            Compass.phase = 0
        end
    end

    if Compass.visible ~= Compass.wasVisible then
        playCompassSfx(Compass.visible)
        Compass.wasVisible = Compass.visible
    end
end

function drawCompass()
    if Compass.visible then

        local function drawit()
            love.graphics.polygon("line", Compass.poly)
            love.graphics.circle("line", Compass.dest.x, Compass.dest.y, 60, 32)
        end

        love.graphics.setColor(255,96,0,255*Compass.opacity*0.5)
        love.graphics.setLineWidth(Compass.width*2.2)
        drawit()

        love.graphics.setColor(255,96,0,255*Compass.opacity)
        love.graphics.setLineWidth(Compass.width)
        drawit()
    end
end
