local IntroTexts = {
    {},
    {" ","Day and Night"," "},
    {},
    {"Kirill","Krysov"},
    {},
    {"Christiaan","Janssen"},
    {},
    {"Berlin Mini","Game Jam"},
    {},
    {"Song:",'"Her name is Cindy"',"by Noisy Sundae","CC-BY-NC-SA"},
    {},
}

local OutroTexts = {
    {},
    {"The","End"},
    {},
    {"Congratulations!"},
    {},
    {"Thanks for","playing"},
    {},
}

function initCredits()
    Credits = {
        timer = 0,
        delay = 2,
        phase = 1,
        skipFirstFrame = true
    }
end

function startCredits()
    Credits.timer = 0
    Credits.phase = 1
end

function updateCredits(dt)
    -- skip first frame (to avoid side effects due to load time)
    if skipFirstFrame then 
        skipFirstFrame = false
        return
    end

    Credits.timer = Credits.timer + dt
    if Credits.timer >= Credits.delay then
        Credits.phase = Credits.phase+1
        Credits.timer = 0
    end

    local currentTexts = Levels.state == LevelStates.credits and IntroTexts or OutroTexts
    if Credits.phase > #currentTexts then
        openIntroScreen()
    end

    if love.keyboard.isDown("escape") then
        openIntroScreen()
    end
end

local function maxWidth(tabl)
    local M = 0
    for _,line in ipairs(tabl) do
        M = math.max(M, fontHuge:getWidth(line))
    end
    return M
end

function drawCredits()
    local currentTexts = Levels.state ==  LevelStates.credits and IntroTexts or OutroTexts
    if Credits.phase <= #currentTexts then
        love.graphics.setColor(255,255,255)
        love.graphics.setFont(fontHuge)
        local tabl = currentTexts[Credits.phase]
        local hh = fontHuge:getHeight()
        local prespace = fontHuge:getWidth(" ")*2
        if #tabl > 0 then
            local w,h = maxWidth(tabl)+prespace,hh * (#tabl + 0.25)
            love.graphics.push()
            love.graphics.scale(screenWidth/w, screenHeight/h)
            for y,line in ipairs(tabl) do
                local ww = fontHuge:getWidth(line)
                love.graphics.print(line,(w-ww)*0.5, (y-1)*hh+4)
            end
            love.graphics.pop()
        end
    end
end
