
function initGameMenu()
    GameMenu = {
        visible = false,
        pos = Vector(screenWidth*0.5 - 200,250),
        sizes = { Vector(400,350), Vector(640,400), Vector(640,400), Vector(440,180) },
        lineHeight = fontBig:getHeight()+8,
        labels = {"resume","restart level","skip level","controls","sound","switch fullscreen","abandon game"},
        labels_controls_1 = {"KEYBOARD"," ","left/right: move","up: jump","down: enter mirror","shift: search mode","esc: menu","next"},
        labels_controls_2 = {"GAMEPAD"," ","left/right: move","button 1: jump","button 2: enter mirror","shoulder button: search mode","start: menu","back"},
        labels_sound = {"Music ON","Sound Effects ON","back"},
        currentOption = 1,
        mode = 1
    }
end

function resetGameMenu()
    GameMenu.currentOption = 1
    GameMenu.mode = 1
end


function openGameMenu()
    GameMenu.currentOption = 1
    GameMenu.mode = 1
    Levels.state = LevelStates.options
    reloadGameSoundMenu()
end

function reloadGameSoundMenu()
    GameMenu.labels_sound[1] = Sound.music.active and "Music ON" or "Music OFF"
    GameMenu.labels_sound[2] = Sound.sfx.active and "Sound Effects ON" or "Sound Effects OFF"
end


function gameMenuEvalKey(key)
        if key == "escape" then
            Levels.state = LevelStates.playing
        end

        if key == "up" then
            if GameMenu.mode == 1 or GameMenu.mode == 4 then
                GameMenu.currentOption = (GameMenu.currentOption + #GameMenu.labels - 2) % #GameMenu.labels + 1
            end
        end
        if key == "down" then
            if GameMenu.mode == 1 or GameMenu.mode == 4 then
                GameMenu.currentOption = (GameMenu.currentOption % #GameMenu.labels) + 1
            end
        end
        if key == "return" or key == "lshift" or key == "rshift" or key == "space" then
            if GameMenu.mode == 1 then
                if GameMenu.currentOption == 1 then
                    Levels.state = LevelStates.playing
                elseif GameMenu.currentOption == 2 then
                    resetLevel()
                    Levels.state = LevelStates.playing
                elseif GameMenu.currentOption == 3 then
                    startLevelSelection()
                    return
                elseif GameMenu.currentOption == 4 then
                    -- controls
                    GameMenu.mode = 2
                    GameMenu.currentOption = #GameMenu.labels_controls_1
                elseif GameMenu.currentOption == 5 then
                    -- sound
                    GameMenu.mode = 4
                    GameMenu.currentOption = 3
                    reloadSoundMenu()
                elseif GameMenu.currentOption == 6 then
                    switchFullscreen()
                elseif GameMenu.currentOption == 7 then
                    openIntroScreen()
                    return
                end
            elseif GameMenu.mode == 2 then
                GameMenu.mode = 3
                GameMenu.currentOption = #GameMenu.labels_controls_1
            elseif GameMenu.mode == 3 then
                GameMenu.mode = 1
                GameMenu.currentOption = 4
            elseif GameMenu.mode == 4 then
                if GameMenu.currentOption == 1 then
                    -- switch music
                    switchMusic()
                    reloadGameSoundMenu()
                elseif GameMenu.currentOption == 2 then
                    -- switch sfx
                    switchSfx()
                    reloadGameSoundMenu()
                elseif GameMenu.currentOption == 3 then
                    GameMenu.mode = 1
                    GameMenu.currentOption = 5
                end
            end
        end
    end

function updateGameMenu(dt)
end

function drawGameMenu()
    if Levels.state == LevelStates.options then
        love.graphics.setFont(fontBig)

        local labels = GameMenu.labels
        if GameMenu.mode == 2 then
            labels = GameMenu.labels_controls_1
        elseif GameMenu.mode == 3 then
            labels = GameMenu.labels_controls_2
        elseif GameMenu.mode == 4 then
            labels = GameMenu.labels_sound
        end

        local ss = GameMenu.sizes[GameMenu.mode]
        local pp = Vector((screenWidth- ss.x) * 0.5, (400 - GameMenu.lineHeight * #labels * 0.5))

        love.graphics.setColor(0,0,0,192)
        love.graphics.rectangle("fill",pp.x,pp.y,ss.x,ss.y)
        love.graphics.setColor(255,255,255)
        love.graphics.setLineWidth(2)
        love.graphics.rectangle("line",pp.x,pp.y,ss.x,ss.y)


        for y,label in ipairs(labels) do
            local w = fontBig:getWidth(label)
            local xx,yy = pp.x + (ss.x - w)*0.5, pp.y + (y-0.5) * GameMenu.lineHeight
            love.graphics.setColor(255,255,255,192)
            if y == GameMenu.currentOption then
                love.graphics.rectangle("fill", xx - 12, yy, w + 20, GameMenu.lineHeight)
                love.graphics.setColor(0,0,0)
            end
            love.graphics.print(label, xx, yy+2)
        end
    end

end