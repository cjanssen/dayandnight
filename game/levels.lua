LevelStates = {
    credits = 1,
    begin = 2,
    loading = 3,
    playing = 4,
    selection = 5,
    options = 6,
    ending = 7,
}

function initLevels()
    Levels = {
        current = 1,
        state = LevelStates.credits,
        saves = {false,false,false,false,false},
        last = 371,
        selection = {
            speed = 300,
            camY = 0,
            vRange = Vector(screenHeight*0.25, screenHeight*0.75),
            row = 11,
        },
        endTimer = 0,
        endDelay = 0.5,
        loadTimer = 0,
    }
    loadLevels()
end

function getMaxLevelNumber()
    return Levels.last
end

function resetLevel()
    restoreMap()
    resetPlayer()
    resetCompass()
    resetGameMenu()
    centerCameraInPlayer()
end

function checkLevel()
    for _,picked in ipairs(player.objectsPicked.list) do
        if not picked then return false end
    end
    return true
end

function setLevel(lvl)
    Levels.state = LevelStates.playing
    Levels.current = lvl
    saveLevels()
    reloadMap()
    -- resetLevel()
    -- startMusic()
end

function levelReady()
    resetLevel()
    startMusic()
end


function nextLevel()
    local levelNr = Levels.current + 1
    setLevel(levelNr)
end

function completeLevel()
    if not Levels.saves[Levels.current] then
        Levels.saves[Levels.current] = true
        local nextUnlock = #Levels.saves+1
        if nextUnlock <= getMaxLevelNumber() then
            Levels.saves[nextUnlock] = false
        end
    end

    if Levels.current < Levels.last then
        nextLevel()
    else
        Levels.state = LevelStates.ending
        startCredits()
    end
end

skipLevel = nextLevel

function startOver()
    setLevel(1)
end

function selectionCoord(lvl)
    local ix = (lvl-1)%Levels.selection.row + 1
    local iy = math.floor((lvl-1)/Levels.selection.row)+1
    return Vector((ix-0.5)*80-4, iy*40 + 16)
end

function getSelectionIndex()
    local ix = math.round((Levels.selection.dest.x + 4)/80 + 0.5)
    local iy = math.round((Levels.selection.dest.y-16) / 40)

    return (iy-1)*Levels.selection.row + ix
end

function getSelectionWidth(posy)
    local maxLevel = #Levels.saves
    local lastPos = selectionCoord(maxLevel)
    if posy < lastPos.y then
        return selectionCoord(Levels.selection.row).x
    else
        return lastPos.x
    end
end

function getSelectionHeight(posx)
    local maxLevel = #Levels.saves
    local lastPos = selectionCoord(maxLevel)

    if posx <= lastPos.x then
        return lastPos.y
    else
        return math.max(56, lastPos.y - 40)
    end
end

function startLevelSelection()
    Levels.state = LevelStates.selection
    Levels.selection.dest = selectionCoord(Levels.current)
    Levels.selection.pos = Levels.selection.dest:copy()
end

function updateLevelKeys(key)
    if Levels.state ~=  LevelStates.selection then return end

    local selectionWidth = getSelectionWidth(Levels.selection.dest.y)
    local selectionHeight = getSelectionHeight(Levels.selection.dest.x)
    if key == "left" then
        Levels.selection.dest.x = Levels.selection.dest.x - 80
        if Levels.selection.dest.x < 36 then
            Levels.selection.dest.y = Levels.selection.dest.y - 40
            if Levels.selection.dest.y < 56 then
                Levels.selection.dest = selectionCoord(#Levels.saves)
            else
                Levels.selection.dest.x = getSelectionWidth(Levels.selection.dest.y)
            end
            Levels.selection.pos = Levels.selection.dest:copy()
        end
    end

    if key == "right" then
        Levels.selection.dest.x = Levels.selection.dest.x + 80
        if Levels.selection.dest.x > selectionWidth then
            Levels.selection.dest.x = 36
            Levels.selection.dest.y = Levels.selection.dest.y + 40
            if Levels.selection.dest.y > getSelectionHeight(Levels.selection.dest.x) then
                Levels.selection.dest = selectionCoord(1)
            end
            Levels.selection.pos = Levels.selection.dest:copy()
        end
    end

    if key == "up" then
        Levels.selection.dest.y = Levels.selection.dest.y - 40
        if Levels.selection.dest.y < 56 then
            Levels.selection.dest.y = selectionHeight
            Levels.selection.pos = Levels.selection.dest:copy()
        end
    end

    if key == "down" then
        Levels.selection.dest.y = Levels.selection.dest.y + 40
        if Levels.selection.dest.y > selectionHeight then
            Levels.selection.dest.y = 56
            Levels.selection.pos = Levels.selection.dest:copy()
        end
    end



    if key == "esc" then
    end

    if key == "return" then
        setLevel(getSelectionIndex())
    end
end

function updateLevelSelection(dt)
    if Levels.state ==  LevelStates.selection then
        -- animation
        if not Levels.selection.pos:isEq(Levels.selection.dest) then
            local diff = (Levels.selection.dest - Levels.selection.pos)
            if diff:mod() > Levels.selection.speed * dt then
                Levels.selection.pos = Levels.selection.pos + diff:norm() * Levels.selection.speed * dt
            else
                Levels.selection.pos = Levels.selection.dest:copy()
            end
        end
    end
end

function updateLevel(dt)
    if Levels.state == LevelStates.playing then
        if checkLevel() and Levels.endTimer == 0 then
            Levels.endTimer = Levels.endDelay
        end

        if Levels.endTimer > 0 then
            Levels.endTimer = Levels.endTimer - dt
            if Levels.endTimer <= 0 then
                Levels.endTimer = 0
                completeLevel()
            end
        end
    elseif Levels.state == LevelStates.loading then
        readThreadInput()
        Levels.loadTimer = Levels.loadTimer - dt
        if Levels.loadTimer < 0 and mapIsReady() then
            Levels.state = LevelStates.playing
            levelReady()
        end
    end
end

function startLastLevel()
    setLevel(Levels.current)
end

local filename = "dayanlev.dat"

local xorarray
local function generateXorArray()
    math.randomseed(27)
    math.random()
    xorarray = {}
    for i=1,256 do
        xorarray[i] = math.random(256)
    end
end

function recode(str)
    if not xorarray then generateXorArray() end

    function myxor (a,b)
      local r = 0
      for i = 0, 31 do
        local x = a / 2 + b / 2
        if x ~= math.floor (x) then
          r = r + 2^i
        end
        a = math.floor (a / 2)
        b = math.floor (b / 2)
      end
      return r
    end
    local ndx = 1
    local ret = ""
    for i=1,string.len(str) do
        ret = ret..string.char(myxor(string.byte(str,i), xorarray[ndx]))
        ndx = (ndx%table.getn(xorarray)) + 1
    end
    return ret
end

function loadLevels()
    function load()
        if love.filesystem.exists(filename) then
            local dataFile = love.filesystem.read(filename)
            if dataFile then
                local unlocked = {loadstring(recode(dataFile))()}
                Levels.saves = {}
                local currentLvl = math.min(unlocked[1], Levels.last)
                Levels.current = currentLvl
                local lastLvl = math.min(unlocked[2], Levels.last)
                for i=1,lastLvl do
                    Levels.saves[i] = true
                end
                for i=3,#unlocked do
                    Levels.saves[unlocked[i]] = false
                end
            end
        end
    end
    local ok = pcall(load)
    if not ok then print("ERROR LOADING GAMEDATA") end
end

function saveLevels()
    local function save()
        local unlockedLevels = { Levels.current }
        local lastFin = 1
        for l,unl in ipairs(Levels.saves) do
            if unl then
                lastFin = l
            else
                table.insert(unlockedLevels, l) 
            end
        end

        table.insert(unlockedLevels, 2, lastFin)

        local ss = "return "
        for _,nr in ipairs(unlockedLevels) do
            ss = ss..nr..","
        end

        love.filesystem.write(filename, recode(ss), ss:len()-1)
    end
    local ok = pcall(save)
    if not ok then print("ERROR SAVING GAMEDATA") end
end

function wipeSaves()
    love.filesystem.remove(filename)
    Levels.saves = {false,false,false,false,false}
    Levels.current = 1
    openIntroScreen()
end

local settingsFilename = "settings.dat"
function saveSettings()
    local function save()
        local ss = "return "..tostring(Sound.music.active)..","..tostring(Sound.sfx.active)..","..tostring(isFullscreen())
        love.filesystem.write(settingsFilename, recode(ss), ss:len())
    end
    local ok = pcall(save)
    if not ok then print("ERROR SAVING SOUND SETTINGS") end
end

function loadSettings()
    function load()
        if love.filesystem.exists(settingsFilename) then
            local dataFile = love.filesystem.read(settingsFilename)
            if dataFile then
                local fs
                Sound.music.active,Sound.sfx.active,fs = loadstring(recode(dataFile))()
                setFullscreen(fs)
            end
        end
    end
    local ok = pcall(load)
    if not ok then print("ERROR LOADING SETTINGS") end
end

function drawLevels()
    if Levels.state ==  LevelStates.selection then
        drawSplash()

        local rx,ry,rw,rh = 50,50,screenWidth-100,screenHeight-100
        local rR = Levels.selection.row
        love.graphics.setColor(0,0,0,192)
        love.graphics.rectangle("fill",rx,ry,rw,rh)
        love.graphics.setColor(255,255,255)
        love.graphics.setLineWidth(2)
        love.graphics.rectangle("line",rx,ry,rw,rh)

        love.graphics.setColor(255,128,64)

        love.graphics.setFont(fontBig)

        local txt = "Select Level"
        local lx = fontBig:getWidth(txt)
        love.graphics.print("Select Level", (screenWidth-lx)*0.5, rx+8)

        local function selectionStencil()
            love.graphics.rectangle("fill",rx,ry+50,rw,rh-60)
        end
        love.graphics.setStencil(selectionStencil)
        
        love.graphics.push()

        local screenY = Levels.selection.pos.y - Levels.selection.camY
        if screenY > Levels.selection.vRange.y then
            Levels.selection.camY = Levels.selection.pos.y - Levels.selection.vRange.y
        elseif screenY < Levels.selection.vRange.x then
            Levels.selection.camY = Levels.selection.pos.y - Levels.selection.vRange.x
        end

        love.graphics.translate(rx,ry-Levels.selection.camY)

        -- level numbers
        for L = 0,#Levels.saves-1 do
            local ix = L%rR + 1
            local iy = math.floor(L/rR)+1
            local pos = Vector((ix-0.5)*80, iy*40 + 10)

            if not Levels.saves[L+1] then
                love.graphics.setColor(255,128,64)
            else
                love.graphics.setColor(255,255,255)
            end
            love.graphics.print(L+1, pos.x+2, pos.y+8)
        end

        -- selection rectangle
        love.graphics.setColor(255,128,64)
        love.graphics.setLineWidth(2)
        love.graphics.rectangle("line", Levels.selection.pos.x, Levels.selection.pos.y, 75, 40)

        love.graphics.pop()

        love.graphics.setStencil()
    elseif Levels.state == LevelStates.loading then
        love.graphics.setColor(255,255,255)
        love.graphics.setFont(fontMid)
        local txt = Levels.current.." / "..Levels.last
        local w = fontMid:getWidth(txt)
        love.graphics.print(txt, (screenWidth - w) * 0.5, 150)
        if Levels.loadTimer < 0 then
            love.graphics.setFont(fontBig)
            local txt = "Loading"
            local pointCount = math.floor((math.abs(Levels.loadTimer)%1.2)/0.3)
            for i=1,pointCount do txt = txt.."." end
            love.graphics.print(txt, 450, 560)
        end
    end
end
