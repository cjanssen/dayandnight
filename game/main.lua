function love.load()
	love.filesystem.load("credits.lua")()
	love.filesystem.load("menu.lua")()
	love.filesystem.load("vector.lua")()
	love.filesystem.load("map.lua")()
	love.filesystem.load("player.lua")()
	love.filesystem.load("camera.lua")()
	love.filesystem.load("background.lua")()
	love.filesystem.load("compass.lua")()
	love.filesystem.load("levels.lua")()
	love.filesystem.load("gamemenu.lua")()
	love.filesystem.load("sound.lua")()

	fontBig = love.graphics.newFont("fnt/Cecily.otf",36)
	love.graphics.setFont(fontBig)
	fontMid = love.graphics.newFont("fnt/Cecily.otf",104)
	fontHuge = love.graphics.newFont("fnt/Cecily.otf",200)

	screenWidth,screenHeight = 1024,768

	initLevels()
	initCredits()
	initMenu()
	initMaps()
	initPlayer()
	initCamera()
	initBackground()
	initCompass()   
	initGameMenu()
	initSound()
	startCredits() 

	-- startLastLevel()
	-- openIntroScreen()
end


function love.update(dt)
	if Levels.state == LevelStates.playing then
		updateLevel(dt)
		updateMap(dt)
		updatePlayer(dt)
		updateCompass(dt)
		updateCamera(dt)
	elseif Levels.state == LevelStates.credits then
		updateCredits(dt)
	elseif Levels.state == LevelStates.begin then
		updateStartMenu(dt)
	elseif Levels.state == LevelStates.selection then
		updateLevelSelection(dt)
	elseif Levels.state == LevelStates.options then
		updateGameMenu(dt)
	elseif Levels.state == LevelStates.ending then
		updateCredits(dt)
	elseif Levels.state == LevelStates.loading then
		updateLevel(dt)
	end

	releaseJoystick()
	updateMusic(dt)
end

function love.draw()
	love.graphics.push()
	drawBands()

	if Levels.state == LevelStates.playing or Levels.state == LevelStates.options then
		drawBackground()

		love.graphics.push()
		love.graphics.scale(camera.scale:unpack())
		love.graphics.translate(-camera.viewPos.x, -camera.viewPos.y)
			drawMap()
			drawCompass()
			drawPlayer()
		love.graphics.pop()
		drawPickedObjects()

		drawGameMenu()
	elseif Levels.state == LevelStates.credits then
		drawCredits()
	elseif Levels.state == LevelStates.begin then
		drawSplash()
		drawStartMenu()
	elseif Levels.state == LevelStates.selection then
		drawLevels()
	elseif Levels.state == LevelStates.loading then
		drawLevels()
	elseif Levels.state == LevelStates.ending then
		drawCredits()
	end

	love.graphics.setStencil()
	love.graphics.pop()
end

function drawBands()
	local dw,dh = screenWidth,screenHeight
	local w,h = love.graphics.getWidth(),love.graphics.getHeight()
	if w == dw and h==dh then return end
	
	local sc = math.min(w/dw,h/dh)

	local x,y = (w/sc-dw)*0.5,(h/sc-dh)*0.5

	local function st()
		love.graphics.rectangle("fill",x,y,dw,dh)
	end
	love.graphics.scale(sc,sc)
	love.graphics.setStencil(st)
	love.graphics.translate(x,y)
end

-- joystick
function love.joystickpressed(joystick, btn)
	if Levels.state ~= LevelStates.playing then love.joystick.isBusy = joystick end
	if btn>=9 then love.keypressed("escape") end
	if btn<=4 then love.keypressed("return") end
end

function love.joystickaxis( joystick, axis, value )
	if Levels.state == LevelStates.playing then
		love.joystick.isBusy = false
	elseif not love.joystick.isBusy then
		love.joystick.isBusy = joystick
		if axis==2 and value < -0.2 then 
			love.keypressed("up")
		elseif axis==2 and value > 0.2 then 
			love.keypressed("down") 
		elseif axis==1 and value < -0.2 then 
			love.keypressed("left")
		elseif axis==1 and value > 0.2 then 
			love.keypressed("right")
		else
			love.joystick.isBusy = false
		end
	end
end

function love.joystickhat( joystick, hat, direction )
	if hat == 1 then
		if Levels.state ~= LevelStates.playing then love.joystick.isBusy = joystick end
		if direction:find("u") then love.keypressed("up") end
		if direction:find("d") then love.keypressed("down") end
	end
end

function releaseJoystick()
	if love.joystick.isBusy then
		if math.abs(love.joystick.isBusy:getAxis(1)) < 0.2 and math.abs(love.joystick.isBusy:getAxis(2)) < 0.2 and not love.joystick.isBusy:isDown(1,2,3,4,5,6,7,8,9,10) then
			love.joystick.isBusy = false
		end
	end
end


function love.keypressed(key)
	if key=="escape" then
		if Levels.state == LevelStates.playing then
			openGameMenu()
			return
		elseif Levels.state == LevelStates.begin then
			if Menu.visible then
				Menu.visible = false
				return
			end
			-- openIntroScreen()
			-- return
		elseif Levels.state == LevelStates.selection then
			openIntroScreen()
		end
		-- love.event.push("quit")
		-- return
	end

	-- level selection
	if Levels.state == LevelStates.begin then
		menuEvalKey(key)
	elseif Levels.state ==  LevelStates.selection then
		updateLevelKeys(key)
	elseif Levels.state == LevelStates.options then
		gameMenuEvalKey(key)
	end
end