function initMaps()
	Maps = {}

	Maps.solids = {
		[0] = false,
		[1] = false,
		[2] = true,
		[3] = true,
		[4] = false, -- mirror
		[5] = false,
	}

	MapThreads = {
		levelChan = love.thread.getChannel("levelNr"),
		mapChan = love.thread.getChannel("maps"),
		thr = nil,
		done = false,
		co = nil,
	}
end

function reloadMap()
	launchMapThread(Levels.current)
	Maps.current = Maps.day
end

function launchMapThread(levelNr)
    MapThreads.thr = love.thread.newThread("procgen.lua")
    
    Levels.state = LevelStates.loading
    MapThreads.done = false
    Levels.loadTimer = 1.5

    MapThreads.thr:start()
    MapThreads.levelChan:push(levelNr)
    MapThreads.co = coroutine.create(co_readThreadInput)
end


function co_readThreadInput()
	while MapThreads.mapChan:getCount() == 0 do
		coroutine.yield()
	end
    local mapsize = MapThreads.mapChan:pop()
    function emptyMap(siz)
        return {
            width = siz[1],
            height = siz[2],
            tilewidth = 64,
            tileheight = 64,
            indices = {}
        }
    end
    local mapday,mapnight = emptyMap(mapsize), emptyMap(mapsize)
    mapday.objLocations = {}

    -- read object locations
    repeat
    	if MapThreads.mapChan:getCount() ~= 0 then
    		local ol = MapThreads.mapChan:pop()
    		table.insert(mapday.objLocations, Vector(unpack(ol)))
    	else
    		coroutine.yield()
    	end
    until #mapday.objLocations == 4

    -- read day cells
    local ix = 1
    repeat
        if MapThreads.mapChan:getCount() ~= 0 then
            mapday.indices[ix] = MapThreads.mapChan:pop()
            ix = ix + 1
        else
            coroutine.yield()
        end
    until ix > mapday.width

    -- read night cells
	ix = 1
    repeat
        if MapThreads.mapChan:getCount() ~= 0 then
            mapnight.indices[ix] = MapThreads.mapChan:pop()
            ix = ix + 1
        else
            coroutine.yield()
        end
    until ix > mapnight.width

    coroutine.yield()
    Maps.day,Maps.night = mapday,mapnight
    processMap(Maps.day)
    processMap(Maps.night)
    decideMapZoom()
end

function mapIsReady()
	return MapThreads.done
end


function readThreadInput()
	if MapThreads.co and not MapThreads.done then
	    if coroutine.status(MapThreads.co) == "suspended" then
	        coroutine.resume(MapThreads.co)
	    end

		if coroutine.status(MapThreads.co) == "dead" then
			MapThreads.co = nil
		    MapThreads.done = true
	    end
	end
end


function processMap(Map)
	Map.totalWidth,Map.totalHeight = Map.width*Map.tilewidth, Map.height*Map.tileheight

	-- prepare sprites
	Map.img = love.graphics.newImage("img/basictiles_exp.png")
	Map.sheet = {}

	for yy=0,Map.img:getHeight()-1,Map.tileheight+2 do
		for xx=0,Map.img:getWidth()-1,Map.tilewidth+2 do
			table.insert(Map.sheet,
				love.graphics.newQuad(xx+1,yy+1, Map.tilewidth, Map.tileheight, Map.img:getWidth(), Map.img:getHeight()))
		end
	end

	-- prepare batch
	makeMapBatch(Map)
end

function decideMapZoom()
	-- make camera zoom dependant on map size
	local altZoom = { 1/1.5, 1/2 }
	local frac = math.max(screenWidth / Maps.day.totalWidth, screenHeight / Maps.day.totalHeight)

	local decidedZoom, diffZoom = 1, math.abs(1-frac)
	for i=1,#altZoom do
		local diff = altZoom[i] - frac
		if diff < diffZoom and diff > 0 then
			decidedZoom = altZoom[i]
			diffZoom = diff
		end
	end

	setZoom(decidedZoom)
end

function restoreMap()
	for i,pos in ipairs(Maps.day.objLocations) do
		if Maps.day.indices[pos.x][pos.y] == 0 and
			Maps.day.indices[pos.x][pos.y-1] == 0 then

			Maps.day.indices[pos.x][pos.y-1] = 8+i
			Maps.day.indices[pos.x][pos.y] = 14+i
		end
	end
	Maps.current = Maps.day
	makeMapBatch(Maps.current)
end

function checkObjectsMissing()
	local objlist = {15,16,17,18}
	for ix=2,Maps.day.width-1 do
		for iy=3,Maps.day.height-1 do
			for ob = #objlist,1,-1 do
				if Maps.day.indices[ix][iy] == objlist[ob] then
					table.remove(objlist,ob)
					if #objlist == 0 then return false end
				end
			end
		end
	end
	return objlist
end

function makeMapBatch(Map)
	Map.batch = love.graphics.newSpriteBatch(Map.img, 10000)

	for ix=1,Map.width do
		for iy=1,Map.height do
			-- if Map.indices[ix][iy+1] == 4 then Map.indices[ix][iy] = 4 end
			if Map.indices[ix][iy] > 1 then
				Map.batch:add(Map.sheet[Map.indices[ix][iy]], (ix-1)*Map.tilewidth, (iy-1)*Map.tileheight)
			end
			if Map.indices[ix][iy] == 1 then
				setPlayerStartPos(Vector(ix,iy))
			end
		end
	end
end

-- function love.livereload()
-- 	reloadMap()
-- end

function updateMap(dt)

end

function checkMapiCollision(pos)
	return Maps.solids[Map.indices[pos.x][pos.y]]
end

function checkMapCollision(pos)
	local ix,iy = math.floor(pos.x/Maps.current.tilewidth)+1,math.floor(pos.y/Maps.current.tileheight)+1

	return Maps.solids[Maps.current.indices[ix][iy]]
end



function flipMap()
	if Maps.current == Maps.day then Maps.current = Maps.night else Maps.current = Maps.day end
	local isDay = Maps.current == Maps.day
	player.jumpAmount = isDay and 850 or 1050
	player.acc = isDay and 8000 or 12000
end

function getTileCoordCorrected(pos)
	local ix,iy = math.floor(pos.x/Maps.current.tilewidth),math.floor(pos.y/Maps.current.tileheight)
	return Vector(ix*Maps.current.tilewidth,iy*Maps.current.tileheight)
end

function checkMapRectCollision(pos,size)
	local topleft,bottomright = pos,pos+size
	local ixf,iyf = math.floor(topleft.x/Maps.current.tilewidth)+1,math.floor(topleft.y/Maps.current.tileheight)+1
	local ixt,iyt = math.floor(bottomright.x/Maps.current.tilewidth)+1,math.floor(bottomright.y/Maps.current.tileheight)+1

	for ix = ixf,ixt do
		for iy=iyf,iyt do
			if Maps.current.indices[ix] and Maps.current.indices[ix][iy] and Maps.solids[Maps.current.indices[ix][iy]] then
				return true
			end
		end
	end

	return false
end

function checkMapMirror(pos,size)
	local topleft,bottomright = pos,pos+size
	local ixf,iyf = math.floor(topleft.x/Maps.current.tilewidth)+1,math.floor(topleft.y/Maps.current.tileheight)+1
	local ixt,iyt = math.floor(bottomright.x/Maps.current.tilewidth)+1,math.floor(bottomright.y/Maps.current.tileheight)+1

	local function isMirror(ndx)
		return ndx==7 or ndx==8 or ndx==13 or ndx==14
	end

	for ix = ixf,ixt do
		for iy=iyf,iyt do
			if isMirror(Maps.current.indices[ix][iy]) then
				return Vector(ix,iy)
			end
		end
	end

	return false
end

function repositionInMirror(ipos)
	if Maps.current.indices[ipos.x][ipos.y] == 7 or Maps.current.indices[ipos.x][ipos.y] == 8 then
		ipos.y = ipos.y+1
	end
	player.pos = Vector((ipos.x - 1) * Maps.current.tilewidth, (ipos.y - 1) * Maps.current.tileheight)
end

local objIndices = {}
local function isObject(ndx)
	if #objIndices>0 then return objIndices[ndx] end
	local ndxs = {9,10,11,12,15,16,17,18}
	for i=1,#ndxs do objIndices[ndxs[i]] = true end
	return objIndices[ndx]
end

function checkMapObject(pos, size)
	if Maps.current ~= Maps.day then return false end
	local topleft,bottomright = pos,pos+size
	local ixf,iyf = math.floor(topleft.x/Maps.current.tilewidth)+1,math.floor(topleft.y/Maps.current.tileheight)+1
	local ixt,iyt = math.floor(bottomright.x/Maps.current.tilewidth)+1,math.floor(bottomright.y/Maps.current.tileheight)+1

	for ix = ixf,ixt do
		for iy=iyf,iyt do
			if Maps.current.indices[ix] and Maps.current.indices[ix][iy] and isObject(Maps.current.indices[ix][iy]) then
				return Maps.current.indices[ix][iy]
			end
		end
	end

	return false
end

function removeMapObject(pos, size, ndx)
	local topleft,bottomright = pos,pos+size
	local ixf,iyf = math.floor(topleft.x/Maps.current.tilewidth)+1,math.floor(topleft.y/Maps.current.tileheight)+1
	local ixt,iyt = math.floor(bottomright.x/Maps.current.tilewidth)+1,math.floor(bottomright.y/Maps.current.tileheight)+1

	local complementary = ndx + 6
	if complementary > 18 then complementary = ndx - 6 end

	for ix = ixf,ixt do
		for iy=iyf,iyt do
			if Maps.current.indices[ix][iy] == ndx or Maps.current.indices[ix][iy] == complementary then
				Maps.current.indices[ix][iy] = 0
				if Maps.current.indices[ix][iy-1] == ndx or Maps.current.indices[ix][iy-1] == complementary then
					Maps.current.indices[ix][iy-1] = 0
				end
				if Maps.current.indices[ix][iy+1] == ndx or Maps.current.indices[ix][iy+1] == complementary then
					Maps.current.indices[ix][iy+1] = 0
				end
			end
		end
	end

	makeMapBatch(Maps.current)

	return false
end

function drawMap()
	love.graphics.setColor(192,192,192)
	love.graphics.draw(Maps.current.batch)
end
