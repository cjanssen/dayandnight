
function initMenu()
    Menu = {
        visible = false,
        pos = Vector(screenWidth*0.5 - 200,250),
        sizes = { Vector(400,350), Vector(640,400), Vector(640,400), Vector(440,180), Vector(600,180) },
        lineHeight = fontBig:getHeight()+8,
        labels = {
            {"play","level select","controls","sound","switch fullscreen","delete data","exit"},
            {"KEYBOARD"," ","left/right: move","up: jump","down: enter mirror","shift: search mode","esc: menu","next"},
            {"GAMEPAD"," ","left/right: move","button 1: jump","button 2: enter mirror","shoulder button: search mode","start: menu","back"},
            {"Music ON","Sound Effects ON","back"},
            {"Confirm delete all progress?","Yes","No"},
        },
        currentOption = 1,
        mode = 1
    }
end

function openIntroScreen()
    Levels.state = LevelStates.begin
    Menu.visible = false
    Menu.labels[1][1] = (Levels.current ~= 1 or #Levels.saves > 5) and "continue" or "play"
    reloadSoundMenu()
end

function reloadSoundMenu()
    Menu.labels[4][1] = Sound.music.active and "Music ON" or "Music OFF"
    Menu.labels[4][2] = Sound.sfx.active and "Sound Effects ON" or "Sound Effects OFF"
end

function menuEvalKey(key)
    if not Menu.visible then
        Menu.visible = true
        Menu.mode = 1
        Menu.currentOption = 1
    else
        if key == "up" then
            if Menu.mode == 1 or Menu.mode == 4 then
                local l = #Menu.labels[Menu.mode]
                Menu.currentOption = (Menu.currentOption + l - 2) % l + 1
            elseif Menu.mode == 5 then
                Menu.currentOption = Menu.currentOption == 3 and 2 or 3
            end
        end
        if key == "down" then
            if Menu.mode == 1 or Menu.mode == 4 then
                local l = #Menu.labels[Menu.mode]
                Menu.currentOption = (Menu.currentOption % l) + 1
            elseif Menu.mode == 5 then
                Menu.currentOption = Menu.currentOption == 3 and 2 or 3
            end
        end
        if key == "return" or key == "lshift" or key == "rshift" or key == "space" then
            if Menu.mode == 1 then
                if Menu.currentOption == 1 then
                    startLastLevel()
                elseif Menu.currentOption == 2 then
                    startLevelSelection()
                elseif Menu.currentOption == 3 then
                    -- controls
                    Menu.mode = 2
                    Menu.currentOption = #Menu.labels[2]
                elseif Menu.currentOption == 4 then
                    -- sound
                    Menu.mode = 4
                    Menu.currentOption = 3
                    reloadSoundMenu()
                elseif Menu.currentOption == 5 then
                    switchFullscreen()
                elseif Menu.currentOption == 6 then
                    Menu.mode = 5
                    Menu.currentOption = 3
                elseif Menu.currentOption == 7 then
                    love.event.push("quit")
                    return
                end
            elseif Menu.mode == 2 then
                Menu.mode = 3
                Menu.currentOption = #Menu.labels[3]
            elseif Menu.mode == 3 then
                Menu.mode = 1
                Menu.currentOption = 3
            elseif Menu.mode == 4 then
                if Menu.currentOption == 1 then
                    -- switch music
                    switchMusic()
                    reloadSoundMenu()
                elseif Menu.currentOption == 2 then
                    -- switch sfx
                    switchSfx()
                    reloadSoundMenu()
                elseif Menu.currentOption == 3 then
                    Menu.mode = 1
                    Menu.currentOption = 4
                end
            elseif Menu.mode == 5 then
                if Menu.currentOption == 2 then
                    wipeSaves()
                elseif Menu.currentOption == 3 then
                    Menu.mode = 1
                    Menu.currentOption = 5
                end
            end
        end
    end
end

function updateStartMenu(dt)
end

function drawStartMenu()
    if Menu.visible then
        love.graphics.setFont(fontBig)

        local labels = Menu.labels[Menu.mode]

        local ss = Menu.sizes[Menu.mode]
        local pp = Vector((screenWidth- ss.x) * 0.5, (400 - Menu.lineHeight * #labels * 0.5))

        love.graphics.setColor(0,0,0,192)
        love.graphics.rectangle("fill",pp.x,pp.y,ss.x,ss.y)
        love.graphics.setColor(255,255,255)
        love.graphics.setLineWidth(2)
        love.graphics.rectangle("line",pp.x,pp.y,ss.x,ss.y)


        for y,label in ipairs(labels) do
            local w = fontBig:getWidth(label)
            local xx,yy = pp.x + (ss.x - w)*0.5, pp.y + (y-0.5) * Menu.lineHeight
            love.graphics.setColor(255,255,255,192)
            if y == Menu.currentOption then
                love.graphics.rectangle("fill", xx - 12, yy, w + 20, Menu.lineHeight)
                love.graphics.setColor(0,0,0)
            end
            love.graphics.print(label, xx, yy+2)
        end
    end

end