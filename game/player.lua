function initPlayer()
	local PlayerStartPos = player and player.startPos or Vector(3,3)

	player = {
		gravity = Vector(0,25000),

		lookRight = true,
		pos = Vector(0,0),
		dir = Vector(0,0),
		vel = Vector(0,0),
		acc = 8000,
		startPos = PlayerStartPos,

		animOffset = Vector(33,32),
		-- animOffset = Vector(0,0),
		size = Vector(52,96),

		drag = 0.7, 
		jumpInertia = 0,
		jumpDrag = 0.85,
		jumpAmount = 850,
		jumpTimer = 0,
		jumpDelay = 0.202,
		touchingFloor = true,
		touchingMirror = false,

		landedTimer = 0,
		landedDelay = 0.25,
		airTimer = 0,
		airDelay = 0.15,

		lookMode = false
	}

	player.pos = player.startPos * 64 - Vector(64,player.size.y)

	loadPlayerFrames()
	prepareObjectsBatch()
end

function setPlayerStartPos(pos)
	player.startPos = pos
end

function prepareObjectsBatch()
	local img = love.graphics.newImage("img/basictiles_exp.png")
	player.objectsPicked = {}
	player.objectsPicked.batch = love.graphics.newSpriteBatch(img,20)
	player.objectsPicked.list = {false,false,false,false}
end

function updateObjectsBatch()
	player.objectsPicked.batch:clear()
	local M = Maps.current
	for i=1,4 do
		if (player.objectsPicked.list[i]) then
			player.objectsPicked.batch:setColor(255,255,255,255)
		else
			player.objectsPicked.batch:setColor(255,255,255,64)
		end
		player.objectsPicked.batch:add(M.sheet[i+8], i* M.tilewidth*0.5, 0, 0, 0.5, 0.5)
		player.objectsPicked.batch:add(M.sheet[i+14], i* M.tilewidth*0.5, 0+M.tileheight*0.5,0,0.5,0.5)
	end
end

function checkObjectsInMap()
	local objsmissing = checkObjectsMissing()
	if objsmissing then
		local mapping = {[15]=1,[16]=2,[17]=3,[18]=4}
		for _,ndx in ipairs(objsmissing) do
			player.objectsPicked.list[mapping[ndx]] = true
		end
	end
end

function loadPlayerFrames()
	local img = love.graphics.newImage("img/charanims.png")
	player.batch = love.graphics.newSpriteBatch(img,10)
	player.sheet = {}

	for j=0,img:getHeight()-1,160 do
		for i=0,img:getWidth()-1,128 do
			table.insert(player.sheet, love.graphics.newQuad(i,j,128,160,img:getWidth(),img:getHeight()))
		end
	end

	player.anims = {
		jump = newAnims({1}),
		idle = newAnims({3,4,5,6}),
		walk = newAnims({7,8}),
		land = newAnims({2})
	}

	player.currentAnim = player.anims.idle.day.right
end

function mapTable(table, func)
	local res = {}
	for i,v in ipairs(table) do
		res[i] = func(v)
	end
	return res
end

function newAnims(frameRefs)
	local shifted = mapTable(frameRefs, function(v) return v + 16 end)
	local day = { right = newAnim(frameRefs), left = newAnim(shifted) }

	local fr2 = mapTable(frameRefs, function(v) return v + 8 end)
	local sh2 = mapTable(fr2, function(v) return v + 16 end)
	local night = { right = newAnim(fr2), left = newAnim(sh2) }

	return { day = day, night = night }
end

function newAnim(frameRefs)
	return {
		frameTime = 0,
		frameDelay = 0.1,
		frameNdx = 1,
		frameRefs = frameRefs,
	}
end

function resetPlayer()
	player.lookRight = true
	player.dir = Vector(0,0)
	player.vel = Vector(0,0)

	player.jumpInertia = 0
	player.jumpAmount = 850
	player.jumpTimer = 0
	player.touchingFloor = true
	player.touchingMirror = false

	player.landedTimer = 0
	player.airTimer = 0

	player.lookMode = false

	player.pos = player.startPos * 64 - Vector(64,player.size.y)

	player.currentAnim = player.anims.idle.day.right
	player.objectsPicked.list = {false,false,false,false}
	checkObjectsInMap()
	updateObjectsBatch()
end

function updatePlayer(dt)
	checkPlayerOutOfScreen()
	playerEvalInputs()
	movePlayer(dt)
	updatePlayerAnims(dt)
	checkObjectTouched()
end

function checkPlayerOutOfScreen()
	if player.pos.x < 0 or player.pos.x > Maps.current.totalWidth or
		player.pos.y < 0 or player.pos.y > Maps.current.totalHeight then
		resetLevel()
	end
end

function playerEvalInputs()
	local keys = {dx = 0, dy = 0, jmp = false, dwn = false}
	player.lookMode = false
	keys = playerEvalJoystick(keys)
	keys = playerEvalKeys(keys)
	applyKeys(keys)
end

function selectPlayerAnim()
	if player.vel.x<0 then player.lookRight = false end
	if player.vel.x>0 then player.lookRight = true end


	local base
	if player.touchingFloor then
		if player.landedTimer > 0 then
			base = player.anims.land
		elseif math.abs(player.vel.x) < 10 then
			base = player.anims.idle
		else
			base = player.anims.walk
		end
	else 
		base = player.anims.jump
	end

	base = Maps.current==Maps.day and base.day or base.night
	base = player.lookRight and base.right or base.left

	player.currentAnim = base

end

function updatePlayerAnims(dt)
	selectPlayerAnim()


	local cA = player.currentAnim
	cA.frameTime = cA.frameTime + dt
	while cA.frameTime >= cA.frameDelay do
		cA.frameNdx = (cA.frameNdx % #cA.frameRefs)+1
		cA.frameTime = cA.frameTime - cA.frameDelay
	end
end

function checkMirrorTouched()
	if not player.mirrorTouched then
		local mirrorPos = checkMapMirror(player.pos, player.size) 
		if mirrorPos then
			flipMap()
			repositionInMirror(mirrorPos)
			player.mirrorTouched = true
			playSwitchSfx()
		end
	end
end

function checkObjectTouched()
	local obj = checkMapObject(player.pos, player.size)
	if obj then 
		removeMapObject(player.pos, player.size, obj)
		local i = obj - 8
		if i > 4 then i = i - 6 end
		player.objectsPicked.list[i] = true
		updateObjectsBatch()
		playPickSfx()
	end
end

function playerEvalJoystick(keys)
	if love.joystick.isBusy then return keys end
	local joysticks = love.joystick.getJoysticks()
	for _,joystick in ipairs(joysticks) do
		if joystick:getButtonCount()>1 then
			if joystick:isDown(5) or joystick:isDown(6) then
                player.lookMode = true
            end
            local function evalAxis(n)
            	local val = joystick:getAxis(n)
            	local thr = player.lookMode and 0.5 or 0.2
            	if math.abs(val) < thr then val = 0 end
            	return val
            end
			keys.dx = evalAxis(1)
			keys.dy = evalAxis(2)
			keys.jmp = joystick:isDown(1) or joystick:isDown(3)
			keys.dwn = joystick:isDown(2) or joystick:isDown(4)

			-- hats
			if joystick:getHatCount() > 0 then
				if joystick:getHat(1):find("l") then keys.dx = -1 end
				if joystick:getHat(1):find("r") then keys.dx = 1 end
				if joystick:getHat(1):find("u") then keys.dy = -1 end
				if joystick:getHat(1):find("d") then keys.dy = 1 end
			end
		end
	end
	return keys
end

function playerEvalKeys(keys)
	if love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift") then
        player.lookMode = true
    end
	if love.keyboard.isDown("left") then
		keys.dx = -1
	end
	if love.keyboard.isDown("right") then
		keys.dx = 1
	end
	if love.keyboard.isDown("up") then
		keys.jmp = true
		keys.dy = -1
	end
	if love.keyboard.isDown("down") then
		keys.dwn = true
		keys.dy = 1
	end

	return keys
end

function applyKeys(keys)
	local dir = Vector(0,0)
	if player.lookMode then
		player.lookMode = Vector(keys.dx, keys.dy)
	else
		dir.x = keys.dx
		if keys.jmp then
			if not player.jumpUsed then
				player.jumpUsed = true
				if player.touchingFloor then
					playJumpSfx()
					player.jumpTimer = player.jumpDelay
				end
			end
		else
			player.jumpTimer = 0
			player.jumpUsed = false
		end
		if keys.dwn then
			checkMirrorTouched()
		else
			player.mirrorTouched = false
		end
	end

	player.dir = dir
end

function movePlayer(dt)
	local oldPos = player.pos:copy()

	if player.jumpTimer > 0 then
		player.jumpTimer = player.jumpTimer - dt
		player.vel.y = player.vel.y - player.jumpAmount
		player.jumpInertia = player.jumpAmount
	elseif player.jumpInertia > 0 then
		player.jumpInertia = player.jumpInertia * math.pow(player.jumpDrag, dt*60)
		if player.jumpInertia > 0.01 then
			player.vel.y = player.vel.y - player.jumpInertia
		else
			player.jumpInertia = 0
		end
	end

	if player.landedTimer > 0 then
		player.landedTimer = player.landedTimer - dt
	end

	-- gravity
	player.vel = player.vel + player.gravity * dt

	player.vel = player.vel * math.pow(player.drag,dt*60) + player.dir * player.acc * dt
	player.pos = player.pos + player.vel * dt

	if not player.touchingFloor then
		player.airTimer = player.airTimer + dt
	end
	player.touchingFloor = false
	if checkMapRectCollision(player.pos,player.size) then
		if player.vel.y > 0 and checkMapRectCollision(Vector(oldPos.x, player.pos.y), player.size) then
			player.pos.y = getTileCoordCorrected(Vector(oldPos.x, player.pos.y+player.size.y)).y - player.size.y - 1
			player.vel.y = 0
			player.touchingFloor = true
		end
		if player.vel.y < 0 and checkMapRectCollision(Vector(oldPos.x, player.pos.y), player.size) then
			player.pos.y = getTileCoordCorrected(Vector(oldPos.x, player.pos.y)).y + Maps.current.tileheight
			player.vel.y = 0
		end
		if player.vel.x ~= 0 and checkMapRectCollision(Vector(player.pos.x, oldPos.y), player.size) then
			player.vel.x = 0
			player.pos.x = oldPos.x
		end
	end

	if player.touchingFloor then
		if player.airTimer >= player.airDelay then
			player.landedTimer = player.landedDelay
			playLandSfx()
		end
		player.airTimer = 0
	end
end


function drawPlayer()
	love.graphics.setColor(255,255,255)
	player.batch:clear()
	player.batch:add(player.sheet[player.currentAnim.frameRefs[player.currentAnim.frameNdx]],
		player.pos.x, player.pos.y, 0, 1, 1, player.animOffset.x, player.animOffset.y)
	love.graphics.draw(player.batch)
end

function drawPickedObjects()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(player.objectsPicked.batch)
end
