---------------------------
-- VECTOR (apparently threads cannot load love files)
function math.sign(a) return a>=0 and 1 or -1 end
-- if min > max, it will return min
function math.clamp(min, val, max) return math.max(min, math.min(val, max)) end
function math.round(a) return math.floor(a+0.5) end

local Vector_proto = {
    copy = function(self) return Vector(self.x,self.y) end,
    norm = function(self) 
        local m = self:mod()
        m = m == 0 and 1 or 1/m
        return m*self
    end,
    mod = function(self)
        return math.sqrt(self*self)
    end,
    angle = function(self)
       return math.atan2(self.y,self.x)
    end,
    isZero = function(self)
        return self.x == 0 and self.y == 0
    end,
    abs = function(self)
        return Vector(math.abs(self.x),math.abs(self.y))
    end,
    sign = function(self)
        return Vector(math.sign(self.x),math.sign(self.y))
    end,
    floor = function(self)
        return Vector(math.floor(self.x),math.floor(self.y))
    end,
    round = function(self)
        return Vector(math.floor(self.x+0.5),math.floor(self.y+0.5))
    end,
    rot = function(self, angle)
        local c,s = math.cos(angle), math.sin(angle)
        return Vector(self.x*c-self.y*s, self.x*s+self.y*c)
    end,
    ortho = function(self)
        return Vector(self.y,-self.x)
    end,
    unpack = function(self)
        return self.x,self.y
    end,
    clamp = function(self,v1,v2)
        return Vector(math.clamp(v1.x, self.x, v2.x), math.clamp(v1.y, self.y, v2.y))
    end,
    -- note: "iseq" instead of overridding the "__eq" operator because
    -- I still want to use vectors as hashes
    isEq = function(self, v2)
        return self.x==v2.x and self.y==v2.y
    end,
    isZero = function(self)
        return self.x==0 and self.y==0
    end
}

local Vector_mt = {
        __index = function(table, key) return Vector_proto[key] end,
        __add = function(lt,rt) return Vector(lt.x+rt.x,lt.y+rt.y) end,
        __sub = function(lt,rt) return Vector(lt.x-rt.x,lt.y-rt.y) end,
        __mul = function(lt,rt)
            if type(lt) == "number" then return Vector(lt*rt.x,lt*rt.y) end
            if type(rt) == "number" then return Vector(lt.x*rt,lt.y*rt) end
            return lt.x*rt.x + lt.y*rt.y
        end,
        __pow = function(lt,rt)
            -- note: there's no easy way to tell the difference between dot product and per-element product
            -- we will use pow for the second one
            return Vector(lt.x*rt.x, lt.y*rt.y)
        end,
        __div = function (tovec, fromvec)
            if type(fromvec) == "number" then return Vector(tovec.x/fromvec, tovec.y/fromvec) end
            if type(tovec) == "number" then return Vector(tovec/fromvec.x, tovec/fromvec.y) end
            -- since division between vectors is not defined, we use the operator for "angle between vectors"
            local v1,v2 = fromvec:norm(),tovec:norm()
            local cosangle = math.acos(math.clamp(-1,v1*v2,1))
            local sinangle = math.asin(math.clamp(-1,v1*v2:ortho(),1))
            return cosangle*math.sign(sinangle)
        end,
        __unm = function(vec) return Vector(-vec.x,-vec.y) end,
    }


function Vector(x,y,data)
    local vec = { x = x or 0, y = y or 0 }
    if data and type(data) == "table" then
        for k,v in pairs(data) do vec[k] = v end
    end
    setmetatable(vec,Vector_mt)
    return vec
end
------------------------------------------------------------------------

function initProcGen()
end

function getBlankMap(w,h)
    local map = {
        width = w,
        height = h,
        tilewidth = 64,
        tileheight = 64,
        indices = {}
    }
    for x = 1,map.width do
        map.indices[x] = {}
        for y = 1,map.height do
            map.indices[x][y] = 0
        end
    end

    for y = 1,map.height do
        map.indices[1][y] = 2
        map.indices[map.width][y] = 2
    end
    for x = 1,map.width do
        map.indices[x][1] = 2
        map.indices[x][map.height] = 2
    end

    return map
end

function countMarks(map)
    local count = 0
    for x=1,map.width do
        for y=1,map.height do
            if map.indices[x][y] ~= 0 then
                count = count + 1
            end
        end
    end
    return count
end

function countMarks3(map)
    local count = 0
    for x=1,map.width do
        for y=1,map.height do
            if map.indices[x][y] ~= 0 then
                count = count + 1
            end
        end
    end

    return count / (map.width*map.height)
end

function countMarks2(mapa,mapb)
    local count = 0
    for _,map in ipairs({mapa,mapb}) do
        for x=1,map.width do
            for y=1,map.height do
                if map.indices[x][y] ~= 0 then
                    count = count + 1
                end
            end
        end
    end
    return count
end

-- 0 -> undefined (available)
-- -1 -> will be fixed gap
-- 2 -> will be fixed wall
-- 3 -> might be obstacle

local function getBresen(pointFrom, pointTo)
    local dx,dy = (pointTo-pointFrom):unpack()
    local points = {}
    local adx,ady = math.abs(dx),math.abs(dy)
    if ady < adx then
        local tg = dy/dx
        local sdx = dx >= 0 and 1 or -1
        for i=0,adx do
            table.insert(points,
                Vector(pointFrom.x + i*sdx,
                  math.round(pointFrom.y + i*tg*sdx) ) )
        end
    else
        local tg = dx/dy
        local sdy = dy >= 0 and 1 or -1
        for i=0,ady do
            table.insert(points,
                Vector(math.round(pointFrom.x + i*tg*sdy),
                  pointFrom.y + i*sdy ) )
        end
    end

    return points
end

local function getLogRnd(max, factor)
    return math.clamp(1, 
            math.ceil(max * (1 + math.log(1-math.random())*factor)), 
            max)
end

local function cleanArea32(map,pos, putZero)
    putZero = putZero and 0 or -1
    for ix = -1,1 do for iy=-1,0 do
        local _ix,_iy = ix+pos.x,iy+pos.y
        if _ix>1 and _ix<map.width and _iy>1 and _iy<map.height then
            map.indices[_ix][_iy] = putZero
        end
    end end
end

function generateBiasMaps()
    local function generateBiasMap()
        local map = {}
        -- 8x8
        local dirs = { Vector(-1,0), Vector(1,0), Vector(0,-1), Vector(0,1) }
        for ix = 1,8 do
            map[ix] = {}
            for iy = 1,8 do
                map[ix][iy] = dirs[math.random(#dirs)]
            end
        end

        return map
    end
    return generateBiasMap(),generateBiasMap()
end

function randn()
    local x1,x2
    local w = 1
    while w >= 1 and w>0 do
        x1 = 2 * math.random() - 1
        x2 = 2 * math.random() - 1
        w = x1*x1 + x2*x2
    end

    w = math.sqrt( (-2 * math.log( w ) / w ) )
    return x1 * w
end

function rande()
    return -math.log(1-math.random()) * 0.125
end

function getBias(biasmap, pos, mapsize)
    local ix,iy = math.floor((pos.x-1)/mapsize.x*8+1),math.floor((pos.y-1)/mapsize.y*8+1)
    return biasmap[ix][iy] + Vector(randn(),randn()) * 0.5
end

function getMaxLevelNumber()
    return 371
end

function getParams(levelNr)
    local difficulty = levelNr / getMaxLevelNumber()


        -- local params = {
    --     initDoorProb = 0.0001,
    --     initialDoors = 9,
    --     daydens = 0.48,
    --     nightdens = 0.68,
    --     width = 48,
    --     height = 32,
    --     minDistToMirrors = 10,
    --     ndxChoiceFactor = 0.2,
    --     doorProbIncrease = 1.12,
    --     ydestChoiceFactor = 0.9,
    --     xdestChoiceFactor = 0.6,
    --     vBiasFactor = 0.5,
    --     hBiasFactor = 0.5,
    --     gapCountWallLimit = 7,
    -- }


    local params = {
        width = 64,
        height = 48,
        daydens = 0.78,
        nightdens = 0.18,
        initDoorProb = 0.0001,
        doorProbIncrease = 1.12,
        initialDoors = 3,
        minDistToMirrors = 20,
        ndxChoiceFactor = 0.42,
        ydestChoiceFactor = 0.99,
        xdestChoiceFactor = 0.99,
        vBiasFactor = 0.5,
        hBiasFactor = 0.5,
        gapCountWallLimitDay = 7,
        gapCountWallLimitNight = 4,
    }

    -- mapsize
    local standardAR, ampAR = 4/3, 16/3
    local ARfactor = math.exp(-4 * math.abs(difficulty - 0.5))
    local aspectRatio = (ampAR * rande() * ARfactor + standardAR)
    if math.random(2)==2 then 
        aspectRatio = 1/aspectRatio 
    end

    local desiredWidth = math.floor(((difficulty * 56 + 14)/4 + randn()*difficulty*1.5) * (standardAR/aspectRatio))*4
    local desiredHeight = math.floor(desiredWidth * aspectRatio / 4) * 4
    local maxW = math.floor(math.sqrt(64*64*3/4/aspectRatio))
    params.width = math.clamp(16, desiredWidth, maxW)
    params.height = math.clamp(12, desiredHeight, math.floor(maxW*aspectRatio))


    local function raisedParab(x) return (1-(x-0.5)*(x-0.5)*4+x)/1.5 end
    local function difficultyDev(f) return math.clamp(0,difficulty + randn() * f,1) end
    params.initDoorProb = 0.01 * raisedParab(difficultyDev(0.1)) + 0.00001
    params.initialDoors = math.round(2 + 15 * raisedParab(difficultyDev(0.05)) * raisedParab(0.5 * difficultyDev(0.08)))
    params.doorProbIncrease = math.max(1.01, 1 + 0.1 * (params.initialDoors + randn()))
    params.minDistToMirrors = params.initialDoors * (1+math.max(0,randn())) + 3

    local baseDens = (params.width * 2 + params.height * 2 + 16) / (params.width*params.height)

    params.daydens = baseDens + (0.95-baseDens) * math.random()
    params.nightdens = 0.95 - (0.95 - baseDens) * math.random() * math.min( params.daydens * 1.4, 1)

    params.ndxChoiceFactor = math.clamp(0.05, 1-rande(), 1)
    params.xdestChoiceFactor = math.clamp(0.05, 1-rande(), 1)
    params.ydestChoiceFactor = math.clamp(0.05, 1-rande(), 1)

    params.vBiasFactor = math.clamp(0.05, randn()/math.pi/2 + 0.5, 0.95)
    params.hBiasFactor = math.clamp(0.05, randn()/math.pi/2 + 0.5, 0.95)

    params.gapCountWallLimitDay = 2 + math.random(7)
    params.gapCountWallLimitNight = 2 + math.random(7)

    return params

end

function generateLevel(levelNr)
    -- print(levelNr)
    math.randomseed(levelNr*math.pi*30)
    local params = getParams(levelNr)

    local mapday = getBlankMap(params.width, params.height)
    local mapnight = getBlankMap(params.width, params.height)
    mapday.objLocations = {}

    local biasday,biasnight = generateBiasMaps()

    local doorProb = params.initDoorProb
    local doorCount = params.initialDoors

    local sideMaxd, vertMaxd = 4,4
    local sideMaxn, vertMaxn = 6,6

    local dayprob = params.daydens / (params.daydens + params.nightdens)
    local origdaydens = params.daydens
    local orignightdens = params.nightdens

    local stackday = {}
    local stacknight = {}
    local initialPos = Vector(math.random(mapday.width-2)+1, math.random(mapday.height-3)+2)
    table.insert(stackday, Vector(initialPos.x, initialPos.y, {day = true}))
    table.insert(stacknight, Vector(initialPos.x, initialPos.y, {day = false}))
    local maxRepeatedPulls = 12

    local mirrors = {}
    

    local function distToMirrors(pos)
        local d = mapday.width+mapday.height
        for _,m in ipairs(mirrors) do
            d = math.min(d, (pos-m):mod())
        end
        return d
    end

    repeat
        local isday = math.random()<dayprob
        local stack = isday and stackday or stacknight
        if #stack == 0 then
            stack = isday and stacknight or stackday
        end

        local ndx = getLogRnd(#stack, params.ndxChoiceFactor)
        local pos = stack[ndx]
        pos.count = (pos.count or 0) + 1
        if pos.count > maxRepeatedPulls then
            table.remove(stack,ndx)
            if #stack <= 0 then
				-- depleted this stack (cannot grow in any direction any more)
                break
            end
        end

        pos.day = isday
        local map = pos.day and mapday or mapnight
        local inversemap = pos.day and mapnight or mapday

        -- insert door?
        if doorCount == params.initialDoors or doorCount > 0 and 
            math.random() < doorProb and 
            distToMirrors(pos)>params.minDistToMirrors and
            mapday.indices[pos.x][pos.y+1] == 0 and
            mapnight.indices[pos.x][pos.y+1] == 0 then

            if doorCount == params.initialDoors then
                pos = pos:clamp(Vector(3,3),Vector(mapday.width-3,mapday.height-3))
            end

            if inversemap.indices[pos.x][pos.y] == 0 and 
                inversemap.indices[pos.x][pos.y+1] == 0 and 
                inversemap.indices[pos.x][pos.y-1] == 0 then

                doorCount = doorCount - 1
                doorProb = params.initDoorProb

                cleanArea32(inversemap,pos)
                cleanArea32(map,pos)

                table.insert(mirrors,pos:copy())

                inversemap.indices[pos.x][pos.y+1] = 2
                map.indices[pos.x][pos.y+1] = 2

                table.insert(stackday, Vector(pos.x, pos.y, {day = true}))
                table.insert(stacknight, Vector(pos.x, pos.y, {day = false}))
            end
        else
            doorProb = doorProb * params.doorProbIncrease

            -- choose destination
            local vertMax = pos.day and vertMaxd or vertMaxn
            local sideMax = pos.day and sideMaxd or sideMaxn

            local ydest = getLogRnd(vertMax*2, params.ydestChoiceFactor) - vertMax
            local xrange = math.floor(sideMax + (ydest + vertMax) * 0.66)
            local xdest = 0
            while xdest == 0 do
                xdest = getLogRnd(xrange*2, params.xdestChoiceFactor)-xrange
            end

            local mapsize = pos.day and Vector(mapday.width,mapday.height) or Vector(mapnight.width,mapnight.height)
            local bias = getBias(pos.day and biasday or biasnight, pos, mapsize)
            ydest = math.clamp(-vertMax,math.round(ydest + bias.y * vertMax * params.vBiasFactor), vertMax)
            xdest = math.clamp(-xrange,math.round(xdest + bias.x * xrange * params.hBiasFactor), xrange)

            local dest = pos + Vector(xdest, ydest)

            dest = dest:clamp(Vector(2,2), Vector(map.width-1,map.height-1))

            dest.day = pos.day

            local destValid = (map.indices[dest.x] and map.indices[dest.x][dest.y+1] == 0)

            if destValid then
                -- check if destination is valid (generate parabola)
                local bresen = getBresen(pos,dest)
                for _,cell in ipairs(bresen) do
                    if not (cell.y > 2 and cell.y < map.height and cell.x > 1 and cell.x < map.width) then
                        destValid = false
                        break
                    end
                    -- if map.indices[cell.x][cell.y] == 2 or map.indices[cell.x][cell.y-1] == 2 or map.indices[cell.x][cell.y-2] == 2 then
                    if map.indices[cell.x][cell.y] == 2 or map.indices[cell.x][cell.y-1] == 2 or (cell.y>3 and map.indices[cell.x][cell.y-2] == 2) then
                        destValid = false
                        break
                    end
                end

                -- if it is, mark path, add to stack
                if destValid then
                    local function setIndexIfZero(ix,iy)
                        if map.indices[ix][iy] == 0 then map.indices[ix][iy] = -1 end
                    end

                    for _,cell in ipairs(bresen) do
                        setIndexIfZero(cell.x,cell.y)
                        setIndexIfZero(cell.x,cell.y-1)
                        setIndexIfZero(cell.x,cell.y-2)
                    end
                    if map.indices[dest.x][dest.y+1] == -1 then
                        map.indices[dest.x][dest.y+1] = 2
                    end

                    table.insert(stack, dest)
                end
            end

            -- account for rejects
            if destValid then
                if pos.day then
                    params.daydens = origdaydens
                else
                    params.nightdens = orignightdens
                end
            else
                local densDecrease = 0.99
                if pos.day then
                    params.daydens = params.daydens * densDecrease
                else
                    params.nightdens = params.nightdens * densDecrease
                end
            end
        end

    until countMarks3(mapday) > params.daydens and countMarks3(mapnight) > params.nightdens

    -- post-process map
    for _,map in ipairs({mapday,mapnight}) do
        local mapcpy = getBlankMap(map.width, map.height)
        local gapLimit = map==mapday and params.gapCountWallLimitDay or params.gapCountWallLimitNight
        for x = 1,map.width do
            for y = 1,map.height do
                if map.indices[x][y] == 0 then
                    local gapcount = 0
                    for _ix = -1,1 do for _iy = -1,1 do
                        gapcount = gapcount + ((map.indices[x+_ix][y+_iy] == -1) and 1 or 0)
                    end end
                    if gapcount > gapLimit then
                        mapcpy.indices[x][y] = 0
                    else
                        mapcpy.indices[x][y] = 2
                    end
                elseif map.indices[x][y] == -1 then
                    mapcpy.indices[x][y] = 0
                else
                    mapcpy.indices[x][y] = map.indices[x][y]
                end
            end
        end
        map.indices = mapcpy.indices
    end

    -- right format of walls
    for x = 1,mapnight.width do
        for y = 1,mapnight.height do
            if mapnight.indices[x][y] == 2 then
                mapnight.indices[x][y] = 3
            end
        end
    end

    -- mirrors
    for _,mirrorPos in ipairs(mirrors) do
        cleanArea32(mapday,mirrorPos, true)
        cleanArea32(mapnight,mirrorPos, true)
        mapday.indices[mirrorPos.x][mirrorPos.y-1] = 7
        mapday.indices[mirrorPos.x][mirrorPos.y] = 13
        mapnight.indices[mirrorPos.x][mirrorPos.y-1] = 8
        mapnight.indices[mirrorPos.x][mirrorPos.y] = 14
    end

    putObjects(mapday,mapnight)

    return mapday,mapnight

end


function putObjects(mapday, mapnight)
    -- analyse reachability

    local function prepareReach(map, wall)
        local m = map.indices
        local reach = {}
        local platlist = {}
        for ix=2,map.width-1 do
            reach[ix] = {}
            for iy=2,map.height-1 do
                if m[ix][iy] ~= wall and m[ix][iy-1] ~= wall and m[ix][iy+1] == wall then
                    local newplat = Vector(ix,iy)
                    reach[ix][iy] = newplat
                    table.insert(platlist, newplat)
                else
                    reach[ix][iy] = false
                end
            end
        end
        return reach,platlist
    end

    -- first, empty
    local reachday,platlistday = prepareReach(mapday,2)
    local reachnight,platlistnight = prepareReach(mapnight,3)

    -- for each "platform", find which other platforms it's connected to
    local sideMaxd, vertMaxd = 4,3
    local sideMaxn, vertMaxn = 6,5

    local function checkReach(map, from, to, wall)
        -- direct LOS * 3
        local reach = true


        local yinc0 = from.x==to.x and -1 or -2
        for xinc = -1,1 do
            reach = true
            local _from,_to = from + Vector(xinc,0), to+Vector(xinc,0)
            local bresen = getBresen(_from,_to)
            for _,cell in ipairs(bresen) do
                local yi = yinc0
                if cell.y == to.y or cell.y == from.y then
                    yi = -1
                end
                for yinc=yi,0 do
                    if not map.indices[cell.x] or map.indices[cell.x][cell.y+yinc] == wall then
                        reach = false
                        break
                    end
                end

                if not reach then break end
            end

            if reach then break end
        end

        -- special case: almost vertical jump (except off by 1)
        if not reach and math.abs(from.x - to.x)== 1 then 
            local upper = to.y<=from.y and to or from
            local lower = upper == to and from or to

            local incx = math.sign(lower.x - upper.x)
            local bresen = getBresen(lower, upper + Vector(incx, -1))
            table.insert(bresen, Vector(upper.x, upper.y-1))
            reach = true
            for _,cell in ipairs(bresen) do
                if not map.indices[cell.x] or map.indices[cell.x][cell.y] == wall then
                    reach = false
                    break
                end
            end
        end

        -- make sure upper are has enough "breathing room"
        if reach then

            local upper = to.y<=from.y and to or from
            local lower = upper == to and from or to
            
            -- completely vertical ascension: don't bother
            if to.y <= from.y and math.abs(from.x-to.x)==0 then
                return reach
            end

            local sideDist = math.ceil(math.abs(from.x-to.x)*0.5)
            local sideDir =  math.sign(from.x - to.x)
            if upper == from then sideDir = -sideDir end

            -- diagonal jumps: make sure that there's space to move around the upper corner
            do
                local extrapush = -1
                if math.abs(from.y-to.y) <= 1 and from.y>=to.y then
                    if math.abs(from.x-to.x)>3 then extrapush = -2 end
                    if math.abs(from.x-to.x)>6 then extrapush = -3 end
                end
                for iy = extrapush,0 do
                    for ix = 1,sideDist do
                        local _ix = upper.x + ix * sideDir
                        local _iy = upper.y + iy
                        if not map.indices[_ix] or map.indices[_ix][_iy] == wall then
                            return false
                        end
                        _ix = lower.x - ix * sideDir
                        _iy = lower.y + iy
                        if not map.indices[_ix] or map.indices[_ix][_iy] == wall then
                            return false
                        end
                    end
                end
            end

            -- diagonal jumps: make sure that there is space to move in the lower platform
            do
                if map.indices[lower.x][lower.y-2] == wall or math.abs(upper.x - lower.x) > 1 then
                    -- if not open above, it must be open to the sides
                    if map.indices[lower.x - sideDir][lower.y] == wall or
                        map.indices[lower.x - sideDir][lower.y-1] == wall or
                        (upper.y ~= lower.y and map.indices[lower.x - sideDir][lower.y-2] == wall) then
                        return false
                    end
                end
            end

            -- long distance jump up (only at night): enough free space in the bottom?
            do
                if from.y-to.y > 2 and math.abs(from.x-to.x)>=sideMaxd+2 and map.indices[from.x + sideDir] and 
                    map.indices[from.x-sideDir][from.y-3] == wall then
                    return false
                end
            end

            -- diagonal jumps: if using the ceiling for L-jump, make sure that the ceiling is not broken
            do
                local extrapush = -1
                if math.abs(from.x-to.x) > 1.5*math.abs(from.y-to.y) and
                    math.abs(from.y-to.y) >= 1 then
                    extrapush = -2
                    if from.y < to.y then
                        if math.abs(from.x-to.x) > 3 then
                            extrapush = -3
                        end
                        sideDist = math.ceil(math.abs(from.x-to.x)*0.65)
                    end
                end

                if math.abs(from.y-to.y) <= 1 and math.abs(from.x-to.x)>2 then extrapush = -2 end

                for iy = extrapush,-1 do
                    local _iy = upper.y + iy
                    local desiredWall = map.indices[upper.x + sideDir][_iy]
                    for ix = 2,sideDist do
                        local _ix = upper.x + ix * sideDir
                        if not map.indices[_ix] or map.indices[_ix][_iy] ~= desiredWall then
                            return false
                        end
                    end
                end
            end
        end

        return reach
    end

    for _,map in ipairs({mapday, mapnight}) do

        local platlist = map==mapday and platlistday or platlistnight
        local reach = map==mapday and reachday or reachnight
        local wall = map==mapday and 2 or 3
        local sideMax = map==mapday and sideMaxd or sideMaxn
        local vertMax = map==mapday and vertMaxd or vertMaxn

        for _,plat in ipairs(platlist) do
            for iy = -vertMax,map.height do
                local incx = 0.33
                if iy<0 then incx = 0.5 end
                local horzMax = sideMax + math.floor((vertMax+iy)*incx)
                for ix = -horzMax,horzMax do
                    if ix==0 and iy==0 then
                        -- ignore
                    else
                        local destcoord = plat+Vector(ix,iy)
                        if reach[destcoord.x] and reach[destcoord.x][destcoord.y] and checkReach(map, plat, destcoord, wall) then
                            table.insert(plat,reach[destcoord.x][destcoord.y])
                        end
                    end
                end
            end
        end
    end

    -- connect mirrors
    for _,plat in ipairs(platlistday) do
        if mapday.indices[plat.x] and mapday.indices[plat.x][plat.y] == 13 and mapnight.indices[plat.x][plat.y] == 14 then
            local rn = reachnight[plat.x][plat.y]
            table.insert(plat, rn)
            table.insert(rn, plat)
        end
    end

 -- find islands (lists of mutually connected platforms)
    local islands = {}
    local function isNeigh(platfrom, platto)
        local function isDirectNeigh(pf,pt)
            for _,rev in ipairs(pt) do
                if rev == pf then return true end
            end
            return false
        end
        if isDirectNeigh(platfrom, platto) then return true end

        if platfrom.isl and platto.isl then
            for _,rev in ipairs(platto.isl) do
                for _,forth in ipairs(platfrom.isl) do
                    if isDirectNeigh(forth,rev) then return true end
                end
            end
        elseif platfrom.isl then
            for _,forth in ipairs(platfrom.isl) do
                if isDirectNeigh(forth, platto) then return true end
            end
        elseif platto.isl then
            for _,rev in ipairs(platto.isl) do
                if isDirectNeigh(platfrom,rev) then return true end
            end
        end
        return false
    end

    -- two-phase: make sure that islands are merged
    -- if the cycles are long, they will not be merged on the first pass, thus the second pass
    for iter=1,2 do
        for _,platlist in ipairs({platlistday,platlistnight}) do
            for _,plat in ipairs(platlist) do
                for _,neigh in ipairs(plat) do
                    -- are they already neighbours?
                    if plat.isl and neigh.isl and plat.isl == neigh.isl then
                        -- nothing
                    elseif isNeigh(plat, neigh) then
                        if plat.isl and neigh.isl and plat.isl ~= neigh.isl then
                            -- merge islands
                            local tomovelist = neigh.isl
                            for _,moves in ipairs(tomovelist) do
                                table.insert(plat.isl, moves)
                                moves.isl = plat.isl
                            end

                            for i=#islands,1,-1 do
                                if islands[i] == tomovelist then
                                    table.remove(islands, i)
                                    break
                                end
                            end
                        elseif plat.isl and not neigh.isl then
                            -- append neigh to plat.isl
                            neigh.isl = plat.isl
                            table.insert(neigh.isl, neigh)
                        elseif neigh.isl and not plat.isl then
                            -- append plat to neigh.isl
                            plat.isl = neigh.isl
                            table.insert(plat.isl, plat)
                        elseif not plat.isl and not neigh.isl then
                            -- create new island for both
                            local newisland = {plat, neigh}
                            plat.isl = newisland
                            neigh.isl = newisland
                            table.insert(islands, newisland)
                        end
                    else
                        -- not neighbours
                        if not plat.isl then
                            -- create plat.isl
                            plat.isl = { plat }
                            table.insert(islands, plat.isl)
                        end
                        if not neigh.isl then
                            -- create neigh.isl
                            neigh.isl = { neigh }
                            table.insert(islands, neigh.isl)
                        end
                    end
                end
            end
        end
    end

    -- necessary for next step: mark each platform as "day" or "night"
    for _,plat in ipairs(platlistday) do
        plat.day = true
    end
    for _,plat in ipairs(platlistnight) do
        plat.day = false
    end

    -- because we want to look at path in reverse, we need to get "backtrack" neighbours of the platforms
    for _,platlist in ipairs({platlistday,platlistnight}) do
        for _,plat in ipairs(platlist) do
            if not plat.backtrack then plat.backtrack = {} end
            for _,neigh in ipairs(plat) do
                if not neigh.backtrack then neigh.backtrack = {} end
                table.insert(neigh.backtrack, plat)
            end
        end
    end

    --- graph of islands (flow of level)
    for _,island in ipairs(islands) do
        island.neighs = island.neighs or {}
        island.incoming = island.incoming or {}
        island.linkout = island.linkout or {}
        island.linkin = island.linkin or {}

        island.hasDay = false
        for _,plat in ipairs(island) do
            if plat.day and mapday.indices[plat.x][plat.y] == 0 then island.hasDay = true end
            for _,neigh in ipairs(plat) do
                if plat.isl ~= neigh.isl then
                    -- store the actual link position
                    plat.isl.linkout = plat.isl.linkout or {}
                    table.insert(plat.isl.linkout, plat)
                    neigh.isl.linkin = neigh.isl.linkin or {}
                    table.insert(neigh.isl.linkin, neigh)

                    -- I can reach another island from here
                    local alreadyconnected = false
                    for _,neighisl in ipairs(island.neighs) do
                        if neighisl == neigh.isl then
                            alreadyconnected = true
                            break
                        end
                    end
                    if not alreadyconnected then
                        table.insert(island.neighs, neigh.isl)
                        if not neigh.isl.incoming then neigh.isl.incoming = {} end
                        table.insert(neigh.isl.incoming, island)
                    end
                end
            end
        end
    end


    -- this graph is directed
    -- there are no loops

    -- choose starting island
    local function getStartingIsland()
        -- start evaluating from biggest island, mark it as "distance 0"
        table.sort(islands, function(a,b) return #a > #b end)

        -- pre-mark all islands with distance -1
        for _,island in ipairs(islands) do island.dist = -1 end

        -- mark each island with distance to biggest
        local taboo,stack = {},{islands[1]}
        islands[1].dist = 0
        while #stack>0 do
            local island = table.remove(stack,1)
            for _,neighisl in ipairs(island.incoming) do
                if not taboo[neighisl] then
                    taboo[neighisl] = true
                    neighisl.dist = island.dist + 1
                    table.insert(stack, neighisl)
                end
            end
        end

        -- sort islands by distance
        table.sort(islands, function(a,b) return a.dist > b.dist end)

        local candidates = {}
        for _,island in ipairs(islands) do
            if island.dist == islands[1].dist and island.hasDay then table.insert(candidates, island) end
        end

        if #candidates == 0 then
            for _,island in ipairs(islands) do
                if island.hasDay and island.dist > -1 then table.insert(candidates,island) end
            end
        end

        -- still nothing? anyone goes then
        if #candidates == 0 then
            for _,island in ipairs(islands) do
                if island.hasDay then table.insert(candidates,island) end
            end
        end

        return candidates[math.random(#candidates)]
    end

    -- same for ending island
    local function getEndingIsland(startingIsland)
        -- pre-mark all islands with distance -1
        for _,island in ipairs(islands) do island.dist = -1 end

        -- mark each island with distance to start
        local taboo,stack = {},{startingIsland}
        startingIsland.dist = 0
        while #stack>0 do
            local island = table.remove(stack,1)
            for _,neighisl in ipairs(island.neighs) do
                if not taboo[neighisl] then
                    taboo[neighisl] = true
                    neighisl.dist = island.dist + 1
                    table.insert(stack, neighisl)
                end
            end
        end

        -- sort islands by distance
        table.sort(islands, function(a,b) return a.dist > b.dist end)

        local candidates = {}
        for _,island in ipairs(islands) do
            if island.dist > startingIsland.dist and island.hasDay then table.insert(candidates, island) end
        end

        if #candidates == 0 then
            for _,island in ipairs(islands) do
                if island.hasDay and island.dist > -1 then table.insert(candidates,island) end
            end
        end

        -- still nothing? anyone goes then
        if #candidates == 0 then
            for _,island in ipairs(islands) do
                if island.hasDay then table.insert(candidates,island) end
            end
        end

        -- bias towards further island
        local ndx = math.clamp(1,math.floor(rande() * #candidates * 1.5),#candidates)
        return candidates[ndx]
        -- return candidates[math.random(#candidates)]
    end

    local startingIsland = getStartingIsland()
    local endingIsland = getEndingIsland(startingIsland)

    local function getStartingPoint()
        -- find links out
        local stack = {}
        local hash = {}

        for _,link in ipairs(startingIsland.linkout) do
            link.distout = 0
            table.insert(stack,link)
            hash[link] = true
        end

        if #startingIsland.linkout == 0 then
            local link = startingIsland[1]
            link.distout = 0
            table.insert(stack,link)
            hash[link] = true
        end

        -- from links out, find distance of all the platforms within starting island
        while #stack > 0 do
            local plat = table.remove(stack, 1)
            -- sort with furthest first
            table.sort(plat,function(a,b) return (a-plat):mod() > (b-plat):mod() end)
            for _,neigh in ipairs(plat) do
                if not hash[neigh] and neigh.isl == startingIsland then
                    hash[neigh] = true
                    neigh.distout = plat.distout + (neigh-plat):mod()
                    table.insert(stack, neigh)
                end
            end
        end

        -- figure out furthest point from link out
        table.sort(startingIsland, function(a,b) return a.distout > b.distout end)

        -- 50% minimum
        local desiredDist = startingIsland[1].distout * 0.5

        -- weed out doors!
        local candidates = {}
        for _,plat in ipairs(startingIsland) do
            if plat.day and plat.distout >= desiredDist and mapday.indices[plat.x][plat.y]==0 then table.insert(candidates, plat) end
        end

        if #candidates == 0 then
            for _,plat in ipairs(startingIsland) do
                if plat.day and mapday.indices[plat.x][plat.y]==0 then table.insert(candidates, plat) end
            end 
        end
        -- still nothing? forget starting island
        if #candidates == 0 then
            for _,plat in ipairs(platlistday) do
                if plat.day and mapday.indices[plat.x][plat.y]==0 then table.insert(candidates, plat) end
            end 
        end

        -- choose a point that is at least 50% of that maximum distance
        return candidates[math.random(#candidates)]
    end

    local startingPoint = getStartingPoint()
    startingIsland = startingPoint.isl


    local function getEndingPoint()
        -- find links out
        local stack = {}
        local hash = {}

        -- different islands: fly away from entry points
        if endingIsland ~= startingIsland and #endingIsland.linkin > 0 then
            for _,link in ipairs(endingIsland.linkin) do
                link.distin = 0
                table.insert(stack,link)
                hash[link] = true
            end
        else
            -- same start and end: fly away from startpoint
            startingPoint.distin = 0
            endingIsland = startingPoint.isl
            table.insert(stack,startingPoint)
            hash[startingPoint] = true
        end

        -- from links in, find distance of all the platforms within starting island
        while #stack > 0 do
            local plat = table.remove(stack, 1)
            -- sort with furthest first
            table.sort(plat,function(a,b) return (a-plat):mod() > (b-plat):mod() end)
            for _,neigh in ipairs(plat) do
                if not hash[neigh] and neigh.isl == endingIsland then
                    hash[neigh] = true
                    neigh.distin = plat.distin + (neigh-plat):mod()
                    table.insert(stack, neigh)
                end
            end
        end

        -- figure out furthest point from link in
        table.sort(endingIsland, function(a,b) return a.distin > b.distin end)

        -- 80% minimum
        local desiredDist = endingIsland[1].distin * 0.8

        -- weed out doors!
        local candidates = {}
        for _,plat in ipairs(endingIsland) do
            if plat.day and plat.distin >= desiredDist and mapday.indices[plat.x][plat.y]==0 then table.insert(candidates, plat) end
        end
        if #candidates == 0 then
            for _,plat in ipairs(endingIsland) do
                if plat.day and mapday.indices[plat.x][plat.y]==0 then table.insert(candidates, plat) end
            end 
        end
        -- still nothing? forget ending island
        if #candidates == 0 then
            for _,plat in ipairs(platlistday) do
                if plat.day and mapday.indices[plat.x][plat.y]==0 then table.insert(candidates, plat) end
            end 
        end

        -- choose a point that is at least 80% of that maximum distance
        return candidates[math.random(#candidates)]

    end

    local endingPoint = getEndingPoint()
    endingIsland = endingPoint.isl


    -- getting locations for the objects
    local locations = {endingPoint}


    local function getGoalHalf(fromPoint, toPoint)

        -- find all platforms reachable from "fromPoint", with distance to them
        do
            local stack = {}
            local hash = {}

            for _,platlist in ipairs({platlistday, platlistnight}) do
                for _,plat in ipairs(platlist) do
                    plat.distout = -1
                end
            end

            fromPoint.distout = 0
            hash[fromPoint] = true
            table.insert(stack, fromPoint)
            while #stack > 0 do
                local plat = table.remove(stack, 1)
                table.sort(plat, function(a,b) return (a-plat):mod() > (b-plat):mod() end)
                for _,neigh in ipairs(plat) do
                    if not hash[neigh] then
                        hash[neigh] = true
                        neigh.distout = plat.distout + (neigh-plat):mod()
                        table.insert(stack, neigh)
                    end
                end
            end
        end

        -- find all platforms that can reach "toPoint", with distance from them
        do
            local stack = {}
            local hash = {}

            for _,platlist in ipairs({platlistday, platlistnight}) do
                for _,plat in ipairs(platlist) do
                    plat.distin = -1
                end
            end

            toPoint.distin = 0
            hash[toPoint] = true
            table.insert(stack, toPoint)
            while #stack > 0 do
                local plat = table.remove(stack, 1)
                table.sort(plat.backtrack, function(a,b) return (a-plat):mod() > (b-plat):mod() end)
                for _,neigh in ipairs(plat.backtrack) do
                    if not hash[neigh] then
                        hash[neigh] = true
                        neigh.distin = plat.distin + (neigh-plat):mod()
                        table.insert(stack, neigh)
                    end
                end
            end
        end

        -- merge distances
        do
            -- disregard night platforms
            for _,plat in ipairs(platlistnight) do
                plat.dist = -1
            end

            -- compute merged distances
            for _,plat in ipairs(platlistday) do
                if plat.distin == -1 or plat.distout == -1 then
                    -- avoid out-of-path platforms
                    plat.dist = -1
                    elseif mapday.indices[plat.x][plat.y] ~= 0 then
                    -- avoid doors/obstacles
                    plat.dist = -1
                else
                    local distToFormerGoals = 0
                    for _,loc in ipairs(locations) do
                        local distToLoc = (plat - loc):mod()
                        if distToLoc < 1 then
                            -- avoid duplicated places!
                            distToFormerGoals = 0
                            break
                        end
                        distToFormerGoals = distToFormerGoals + distToLoc
                    end
                    plat.dist = plat.distin * plat.distout * distToFormerGoals
                end
            end
        end


        -- candidates are: in daylight, with at least 80% of the maximum distance
        table.sort(platlistday, function(a,b) return a.dist > b.dist end)
        local desiredDist = platlistday[1].dist * 0.8

        local candidates = {}
        for _,plat in ipairs(platlistday) do
            if plat.dist >= desiredDist then table.insert(candidates,plat) end
        end

        local result = candidates[math.random(#candidates)]
        table.insert(locations, result)
        return result
    end

    -- subdivide, for finding points in-between
    local goalHalf = getGoalHalf(startingPoint, endingPoint)
    local goalBefore = getGoalHalf(startingPoint, goalHalf)
    local goalAfter = getGoalHalf(goalHalf, endingPoint)


    -- put the items in the designated spots, random order
    local function insertObject(ndxTop, ndxBottom)
        local loc = table.remove(locations,math.random(#locations))
        mapday.indices[loc.x][loc.y-1] = ndxTop
        mapday.indices[loc.x][loc.y] = ndxBottom
        table.insert(mapday.objLocations, loc)
    end

    insertObject( 9,15)
    insertObject(10,16)
    insertObject(11,17)
    insertObject(12,18)
    
    -- finally, set the starting point in the map
    mapday.indices[startingPoint.x][startingPoint.y] = 1
end


-------------------------------------------------------

function doProcess()
    local chanin = love.thread.getChannel("levelNr")
    local chanout = love.thread.getChannel("maps")

    local lvlNr = chanin:demand()


    local mapday,mapnight = generateLevel(lvlNr)
    -- PUSH only accepts flat tables, so here we have to serialize the data

    -- first send map size {width,height}
    chanout:push({mapday.width, mapday.height})

    -- send objects
    for i=1,4 do
        chanout:push({mapday.objLocations[i].x,mapday.objLocations[i].y})
    end

    -- then send each column in a loop, for each map
    for ix=1,#mapday.indices do
        chanout:push(mapday.indices[ix])
    end
    for ix=1,#mapnight.indices do
        chanout:push(mapnight.indices[ix])
    end

end
doProcess()