function initSound()
    Sound = { 
        music = { 
            active = true,
            day = love.audio.newSource("snd/dnb_normal.ogg","stream"),
            night = love.audio.newSource("snd/dnb_cave.ogg","stream"),
        }, 
        sfx = {
            active = true,
            compassOn = false,
            compass = love.audio.newSource("snd/compass.ogg"),
            jump = love.audio.newSource("snd/jump.ogg"),
            land = love.audio.newSource("snd/land.ogg"),
            pick = love.audio.newSource("snd/pick.ogg"),
            switch = love.audio.newSource("snd/switch.ogg"),
            switch_cav = love.audio.newSource("snd/switch_cave.ogg"),
            jump_cav = love.audio.newSource("snd/jump_cave.ogg"),
            land_cav = love.audio.newSource("snd/land_cave.ogg"),
        },
    }

    Sound.music.day:setLooping(true)
    Sound.music.night:setLooping(true)
    Sound.sfx.compass:setLooping(true)
    loadSettings()
end

function startMusic()
    if not Sound.music.day:isPlaying() then
        Sound.music.day:play()
    end
    if not Sound.music.night:isPlaying() then
        Sound.music.night:play()
    end
end

function pauseMusic()
    Sound.music.day:pause()
    Sound.music.night:pause()
end

function continueMusic()
    Sound.music.day:play()
    Sound.music.night:play()
end

function switchMusic()
    Sound.music.active = not Sound.music.active
    saveSettings()

    if Sound.music.active then
        continueMusic()
    else
        pauseMusic()
    end
end

function switchSfx()
    Sound.sfx.active = not Sound.sfx.active
    saveSettings()

    -- compass
    if Sound.sfx.active and Sound.sfx.compassOn then
        Sound.sfx.compass:play()
    else
        Sound.sfx.compass:pause()
    end
end

function updateMusic(dt)

    if (Levels.state == LevelStates.playing or Levels.state == LevelStates.options) and Sound.music.active then
        continueMusic()
        if Maps.current == Maps.night then
            Sound.music.day:setVolume(0)
            Sound.music.night:setVolume(1)
        else
            Sound.music.day:setVolume(1)
            Sound.music.night:setVolume(0)
        end
    else
        pauseMusic()
    end
end


function playCompassSfx(active)
    Sound.sfx.compassOn = active
    if Sound.sfx.active then
        if Sound.sfx.compassOn then
            Sound.sfx.compass:rewind()
            Sound.sfx.compass:play()
        else
            Sound.sfx.compass:pause()
        end
    end
end

function playJumpSfx()
    if Sound.sfx.active then
        if Maps.current == Maps.day then
            Sound.sfx.jump:rewind()
            Sound.sfx.jump:play()
        else
            Sound.sfx.jump_cav:rewind()
            Sound.sfx.jump_cav:play()
        end
    end
end

function playLandSfx()
    if Sound.sfx.active then
        if Maps.current == Maps.day then
            Sound.sfx.land:rewind()
            Sound.sfx.land:play()
        else
            Sound.sfx.land_cav:rewind()
            Sound.sfx.land_cav:play()
        end
    end
end

function playSwitchSfx()
    if Sound.sfx.active then
        if Maps.current == Maps.day then
            Sound.sfx.switch:rewind()
            Sound.sfx.switch:play()
        else
            Sound.sfx.switch_cav:rewind()
            Sound.sfx.switch_cav:play()
        end
    end
end   

function playPickSfx()
    if Sound.sfx.active then
        if Maps.current == Maps.day then
            Sound.sfx.pick:rewind()
            Sound.sfx.pick:play()
        end
    end
end
